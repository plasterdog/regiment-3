<?php
//JMC - backend-experience

// JMC- unregister widgets selectively
 function unregister_default_widgets() {
     unregister_widget('WP_Widget_Pages');
     unregister_widget('WP_Widget_Calendar');
     unregister_widget('WP_Widget_Categories');
     unregister_widget('WP_Widget_Meta');
     unregister_widget('WP_Widget_Archives');
     unregister_widget('WP_Widget_Recent_Posts');
     unregister_widget('WP_Widget_Recent_Comments');
     unregister_widget('WP_Widget_RSS');
     unregister_widget('WP_Widget_Tag_Cloud');
 }
 add_action('widgets_init', 'unregister_default_widgets', 11);

//* -JMC-Replace WordPress login logo with your own
add_action('login_head', 'b3m_custom_login_logo');
function b3m_custom_login_logo() {
echo '<style type="text/css">
h1 a { background-image:url('.get_stylesheet_directory_uri().'/images/login.png) !important; background-size: 250px 150px !important;height: 150px !important; width: 250px !important; margin-bottom: 0 !important; padding-bottom: 0 !important; }
.login form { margin-top: 10px !important; }
</style>';
}

// JMC- custom footer message
function modify_footer_admin () {
  echo 'Themed and configured by Wow! What a great website! ';
  echo 'CMS Powered by WordPress';
}
add_filter('admin_footer_text', 'modify_footer_admin');

//https://css-tricks.com/snippets/wordpress/apply-custom-css-to-admin-area/
add_action('admin_head', 'dashboard_cleanup');

function dashboard_cleanup() {
  echo '<style>

    .metabox-holder .postbox-container .empty-container {
    border: none transparent;
    height: 10px!important;
    position: relative;
    min-height:10px!important;
}
@media only screen and (min-width: 900px) {
     #dashboard-widgets .postbox-container:first-of-type {
        width:33% !important;
    }  

   #dashboard-widgets .postbox-container:first-of-type .postbox .inside {
    padding: 0 12px 12px;
    line-height: 1.4em;
    font-size: 13px;
}
 
    #dashboard-widgets .postbox-container {
        width:66%!important;        
    }

    #dashboard-widgets .postbox-container .postbox .inside {
    padding: 0 6em 3em 3em;
}
}
  </style>';
}
