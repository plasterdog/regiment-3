<?php
/*
*Template Name: Home Page
 * @package regiment-secondhero
 */


get_header(); ?>
<div id="header-bump"></div>

			<?php if ( get_field( 'page_hero_image' ) ): ?>
<div id="home-hero-top">		
<img src="<?php echo esc_url( get_field( 'page_hero_image' ) ); ?>"/>	
<div class="home-hero-content">
	<div class="home-hero-text-section">
<h1><?php the_field('home_hero_title'); ?></h1>
<h2><?php the_field('home_hero_sub_title'); ?></h2>
<p> <?php the_field('home_hero_text_excerpt'); ?></p>
</div><!--ends home hero text section -->
<h5 class="top-cta-button"><a href="<?php the_field('home_hero_call_to_action_target'); ?>"> <?php the_field('home_hero_call_to_action'); ?></a>
</div><!-- ends secondhero content -->

</div><!-- ends secondhero top -->
<?php endif; ?>	
<div id="page" class="hfeed site">
<?php if (! get_field( 'page_hero_image' ) ): ?>
<div id="big-header-bump"></div>
<?php endif; ?>	
	<div id="content" class="site-content" >
	<div id="primary" class="full-content-area">
		<main id="main" class="full-site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'regiment-secondhero' ),
				'after'  => '</div>',
			) );
		?>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

	
	<div class="clear" style="height:2em;"></div>

<?php get_footer('home'); ?>
