<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package regiment-secondhero
 */


get_header(); ?>
<div id="header-bump"></div>


		<div id="page" class="hfeed site">

			<?php if ( get_field( 'page_hero_image' ) ): ?>
					
					<div id="hero-top">		
					<img src="<?php echo esc_url( get_field( 'page_hero_image' ) ); ?>"/>	

					<?php if( get_field('show_title') == 'show' ): ?>

<div class="hero-caption">
					<?php if( get_field('alternate_hero_title') ): ?>
						<h1><?php the_field('alternate_hero_title'); ?></h1>
					<?php endif; ?>

					<?php if(! get_field('alternate_hero_title') ): ?>
						<h1><?php the_title(); ?></h1>
					<?php endif; ?>
					<?php if( get_field('alternate_hero_sub_title') ): ?>
					<p><?php the_field('alternate_hero_sub_title'); ?></p>
					<?php endif; ?>
<div class="clear"></div>
</div>
					<?php endif; ?><!-- the select clause -->

					</div>

			<?php endif; ?>	

			<?php if (! get_field( 'page_hero_image' ) ): ?>
					<div id="big-header-bump"></div>
			<?php endif; ?>	

	<div id="content" class="site-content" >
	<div id="primary" class="full-content-area">
		<main id="main" class="full-site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		
	<?php if ( !get_field( 'page_hero_image' ) ): ?>

		<?php if( get_field('show_title') == 'show' ): ?>

					<?php if( get_field('alternate_hero_title') ): ?>
						<h1><?php the_field('alternate_hero_title'); ?></h1>
					<?php endif; ?>

					<?php if(! get_field('alternate_hero_title') ): ?>
						<h1><?php the_title(); ?></h1>
					<?php endif; ?>

					<p><?php the_field('alternate_hero_sub_title'); ?></p>


		<?php endif; ?><!-- the select clause -->

	<?php endif; ?>	

	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'regiment-secondhero' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<?php edit_post_link( __( 'Edit', 'regiment-secondhero' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->



			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

	
	<div class="clear" style="height:2em;"></div>

<?php get_footer(); ?>
